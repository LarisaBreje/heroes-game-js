class Hero {
  constructor(name, hp) {
    this.name = name;
    this.hp = hp;
    this.canFly = false;
    this.shield = false;
  }
  attacked(damage) {
    let flied = false;
    if (this.canFly) {
      let chance = Math.random();
      if (chance > 0.5) {
        flied = true;
        console.log(this.name + ' flew away');
        // Utilities.sleep(1000);
        damage = 0;
      }
    }
    if (this.shield && !flied) {
      damage *= 0.8;
      console.log(this.name + ' defends with a shield');
      // Utilities.sleep(1000);
    }
    this.hp -= damage;
    console.log(
      this.name +
        ' has been attacked. HP reduced by ' +
        damage +
        '. HP remaining ' +
        this.hp
    );
    // Utilities.sleep(1000);
  }
}

class Dwarf extends Hero {
  constructor(name, hp) {
    super(name, hp);
    this.shield = true;
  }
  attack(otherHero) {
    let damage = 150;
    console.log(this.name + '  attacked with damage ' + damage + '.');
    // Utilities.sleep(1000);
    otherHero.attacked(damage);
  }
}
class Sprite extends Hero {
  constructor(name, hp) {
    super(name, hp);
    this.canFly = true;
  }
  attack(otherHero) {
    let damage = 110;
    console.log(this.name + '  attacked with damage ' + damage + '.');
    // Utilities.sleep(1000);
    otherHero.attacked(damage);
  }
}
class Dragon extends Hero {
  constructor(name, hp) {
    super(name, hp);
    this.shield = true;
    this.canFly = true;
  }
  attack(otherHero) {
    let damage = 200;
    console.log(this.name + '  attacked with damage ' + damage + '.');
    // Utilities.sleep(1000);
    otherHero.attacked(damage);
  }
}

class Fight {
  constructor(hero1, hero2) {
    this.hero1 = hero1;
    this.hero2 = hero2;
    this.turn = 0;
  }
  performAttack() {
    if (this.turn === 0) {
      this.hero1.attack(this.hero2);
    } else {
      this.hero2.attack(this.hero1);
    }
  }
  changeTurn() {
    this.turn = 1 - this.turn;
  }
  findWinner() {
    let winner = document.getElementById('winner-span');
    Utilities.sleep(2000);
    if (this.hero1.hp > 0) {
      winner.innerHTML =
        this.hero1.name + ' won with ' + this.hero1.hp + ' hp left.';
      console.log(this.hero1.name + ' won with ' + this.hero1.hp + ' hp left.');
    } else if (this.hero2.hp > 0) {
      winner.innerHTML =
        this.hero2.name + ' won with ' + this.hero2.hp + ' hp left.';
      console.log(this.hero2.name + ' won with ' + this.hero2.hp + ' hp left.');
    } else {
      winner.innerHTML = 'No heroes left alive!';
      console.log('No heroes left alive!');
    }
  }
  go() {
    do {
      this.performAttack();
      this.changeTurn();
    } while (this.hero1.hp > 0 && this.hero2.hp > 0);
    this.findWinner();
  }
}

class Utilities {
  static sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }
}
// let dwarf = new Dwarf('Utti', 10);
// let sprite = new Sprite('Stewey', 15);
// let dragon = new Dragon('Ruffy', 18);

// let epicFight = new Fight(dwarf, dragon);
// epicFight.go();
