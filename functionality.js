let showHeroesBtn = document.querySelector('#btn');
let heroesContainer = document.querySelector('#container');
let startFightBtn = document.querySelector('#btn-start');
let newGameBtn = document.querySelector('#btn-new');
console.log(newGameBtn);
let winnerBox = document.querySelector('#winner-span');
showHeroesBtn.addEventListener('click', showHeroesFunc);

function showHeroesFunc() {
  heroesContainer.classList.add('d-flex');
  showHeroesBtn.classList.add('d-none');
  startFightBtn.classList.add('d-block');
}

let selectedHeroes = 0;

let selectHero1 = document.querySelector('#hero1');
let selectHero2 = document.querySelector('#hero2');
let selectHero3 = document.querySelector('#hero3');
let isHero1Selected = false;
let isHero2Selected = false;
let isHero3Selected = false;
selectHero1.addEventListener('click', selectHeroFunc1);
selectHero2.addEventListener('click', selectHeroFunc2);
selectHero3.addEventListener('click', selectHeroFunc3);
function selectHeroFunc1() {
  isHero1Selected = selectHero(isHero1Selected, selectHero1);
}
function selectHeroFunc2() {
  isHero2Selected = selectHero(isHero2Selected, selectHero2);
}
function selectHeroFunc3() {
  isHero3Selected = selectHero(isHero3Selected, selectHero3);
}
function selectHero(isHeroSelected, selectHeroBtn) {
  if (isHeroSelected) {
    selectedHeroes--;
    selectHeroBtn.classList.remove('select-hero');
    return false;
  } else {
    if (selectedHeroes < 2) {
      selectHeroBtn.classList.add('select-hero');
      selectedHeroes++;
      return true;
    }
  }
}
startFightBtn.addEventListener('click', startGameVisual);

function startGameVisual() {
  if (selectedHeroes != 2) {
    alert('Please select two heroes to start the game!');
    return;
  }
  heroesContainer.classList.add('d-none');
  startFightBtn.style.visibility = 'hidden';
  winnerBox.classList.add('d-flex');
  winnerBox.innerHTML = 'Fighting ⚔️';
  x.a = 42;
}

function startGameFunc() {
  setTimeout(() => {
    let dwarf = false;
    let sprite = false;
    let dragon = false;

    if (isHero1Selected) {
      dwarf = new Dwarf('Utti', 1200);
    }
    if (isHero2Selected) {
      sprite = new Sprite('Stewey', 800);
    }
    if (isHero3Selected) {
      dragon = new Dragon('Ruffy', 1700);
    }
    if (!dwarf) {
      let epicFight = new Fight(sprite, dragon);
      epicFight.go();
      newGameBtn.classList.add('d-flex');
      return;
    }
    if (!sprite) {
      let epicFight = new Fight(dwarf, dragon);
      epicFight.go();
      newGameBtn.classList.add('d-flex');
      return;
    }
    if (!dragon) {
      let epicFight = new Fight(dwarf, sprite);
      epicFight.go();
      newGameBtn.classList.add('d-flex');
      return;
    }
  }, 2000);
}

x = {
  aInternal: 10,
  aListener: function (val) {},
  set a(val) {
    this.aInternal = val;
    this.aListener(val);
  },
  get a() {
    return this.aInternal;
  },
  registerListener: function (listener) {
    this.aListener = listener;
  },
};
x.registerListener(function (val) {
  startGameFunc();
});
